FROM python:latest

COPY ./ /usr/src/app/
RUN ls /usr/src/app/

RUN pip install --no-cache-dir -r /usr/src/app/requirements.txt
